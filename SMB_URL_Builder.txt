%%[
set @pageID = RequestParameter('pageID')
set @sessionID = RequestParameter('sessionID')



if (@pageID == '13490') then /* Solutions_Finder_Page00 */
    set @url = MicrositeURL(@pageID, 'SessionID', @sessionID)
endif



if (@pageID == '13492') then /* Solutions_Finder_Page01 */
    set @url = MicrositeURL(@pageID, 'SessionID', @sessionID,
'radioVal', RequestParameter('radioVal'))
endif



if (@pageID == '13493') then /* Solutions_Finder_Page02 */
    set @url = MicrositeURL(@pageID,'SessionID', @sessionID,'radioVal', RequestParameter('radioVal'))
endif



if (@pageID == '13494') then /* Solutions_Finder_Page03 */
    set @url = MicrositeURL(@pageID,'SessionID', @sessionID,'sliderVal', RequestParameter('sliderVal'))
endif



if (@pageID == '13495') then /* Solutions_Finder_Page04 */
   set @url = MicrositeURL(@pageID,'SessionID', @sessionID,'radioVal', RequestParameter('radioVal'))
endif



if (@pageID == '13496') then /* Solutions_Finder_Page05 */
    set @url = MicrositeURL(@pageID,
'SessionID', @sessionID,
'choice_1', RequestParameter('choice_1'),
'choice_2', RequestParameter('choice_2'),
'choice_3', RequestParameter('choice_3'),
'choice_4', RequestParameter('choice_4'),
'choice_5', RequestParameter('choice_5'),
'choice_6', RequestParameter('choice_6'),
'choice_7', RequestParameter('choice_7'),
'choice_8', RequestParameter('choice_8'),
'choice_9', RequestParameter('choice_9'),
'choice_10', RequestParameter('choice_10')
    )
endif




/* temp default */
if Empty(@pageID) or Empty(@url) then
    set @url = MicrositeURL('13490', 'SessionID', @sessionID)
endif


Output(v(@url))


]%%